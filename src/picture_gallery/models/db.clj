(ns picture-gallery.models.db
  (:require [clojure.java.jdbc :as sql])
  (:import java.sql.DriverManager))

(def db {:classname   "org.sqlite.JDBC",
         :subprotocol "sqlite",
         :subname     "db.sq3"})

(defmacro with-db [f & body]
  `(sql/with-connection ~db (~f ~@body)))

(defn create-user [user]
  (with-db sql/insert-record :users user))

(defn get-user [id]
  (with-db sql/with-query-results
    res ["SELECT * FROM users WHERE id = ?" id] (first res)))

(defn get-all-users []
  (with-db sql/with-query-results
    res ["SELECT * FROM users"] (doall res)))

(defn add-image [userid name]
  (with-db sql/transaction
    (if (sql/with-query-results
          res
          ["SELECT userid FROM images WHERE userid = ? and name = ?" userid name]
          (empty? res))
      (sql/insert-record :images {:userid userid :name name})
      (throw
        (Exception. "You have already uploaded an image with the same name")))))

(defn delete-user [userid]
  (with-db sql/delete-rows :users ["id=?" userid]))

(defn images-by-user [userid]
  (with-db sql/with-query-results
    res ["SELECT * FROM images WHERE userid = ?" userid] (doall res)))

(defn get-gallery-previews []
  (with-db sql/with-query-results
    res
    ["SELECT rowid, img.userid userid, img.name name, COUNT(*) num
      FROM images img JOIN (SELECT rowid, userid, name FROM images) img1
	                    ON img.userid = img1.userid AND img.rowid >= img1.rowid
      GROUP BY img.userid, img.rowid
      HAVING COUNT (*) <= 1
      ORDER BY img.userid, img.rowid"]
    (doall res)))

(defn delete-image [userid name]
  (with-db sql/delete-rows
  :images ["userid=? and name=?" userid name]))
