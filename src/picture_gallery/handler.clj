(ns picture-gallery.handler
  (:require [compojure.core :refer [defroutes routes]]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [picture-gallery.routes.home :refer [home-routes]]
            [noir.util.middleware :as noir-middleware]
            [noir.session :as session]
            [picture-gallery.models.schema :as schema]
            [picture-gallery.routes.auth :refer [auth-routes]]
            [picture-gallery.routes.upload :refer [upload-routes]]
            [picture-gallery.routes.gallery :refer [gallery-routes]]
            [ring.middleware.session.memory :refer [memory-store]]
            [noir.validation :refer [wrap-noir-validation]]))

(defn init []
  (println "picture-gallery is starting")
  (if-not (.exists (java.io.File. "./db.sq3"))
    (do
      (schema/create-user-table)
      (schema/create-images-table))))

(defn destroy []
  (println "picture-gallery is shutting down"))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found"))

(defn user-page [_]
  (session/get :user))

#_(def app (noir-middleware/app-handler
             [auth-routes
              home-routes
              upload-routes
              gallery-routes
              app-routes]
             :ring-defaults {:cookie    true
                             :security  {:anti-forgery false}}))

(def app
  (->
    (handler/site
      (routes auth-routes home-routes upload-routes gallery-routes app-routes))
    (session/wrap-noir-session
      {:store (memory-store)})
    (wrap-noir-validation)
    (noir-middleware/wrap-access-rules [user-page])))
