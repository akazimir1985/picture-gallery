(defproject picture-gallery "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.0"]
                 [hiccup "1.0.5"]
                 [ring-server "0.4.0"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [org.xerial/sqlite-jdbc "3.7.2"]
                 [lib-noir "0.9.9"]
                 [environ "0.4.0"]]
  :plugins [[lein-ring "0.8.12"]
            [lein-environ "0.4.0"]]
  :ring {:handler picture-gallery.handler/app
         :init picture-gallery.handler/init
         :destroy picture-gallery.handler/destroy
         :auto-reload? true
         :auto-refresh? true}
  :profiles
  {:uberjar {:aot :all}
   :production
   {:ring {:open-browser? false,
           :stacktraces? false,
           :auto-reload? false}
    :env {:port 3000
          :galleries-path "galleries"}}
   :dev
   {:dependencies [[ring-mock "0.1.5"]
                   [ring/ring-devel "1.3.1"]]
    :env {:port 3000
          :galleries-path "galleries"}}})
